// Read name and age

#include "std_lib_facilities.h"

int main()
{
    cout << "Please enter your name and age:\n";

    string first_name = "???";
    double age = 0;
    double age_months = 0;

    cin >> first_name >> age;

    age_months = age * 12;

    cout << "Hello, " << first_name << "(age " << age_months << " months)\n";
}