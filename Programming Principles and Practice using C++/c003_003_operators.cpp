#include "std_lib_facilities.h"

int main()
{
    cout << "Please enter a floating-point value: ";

    double n;
    cin >> n;
    cout << "n == " << n << "\n";
    cout << "n + 1 == " << n + 1 << "\n";
    cout << "3 x n == " << 3 * n << "\n";
    cout << "n ^ 2 == " << n * n << "\n";
    cout << "n / 2 == " << n / 2 << "\n";
    cout << "square root of n == " << sqrt(n) << "\n";
    cout << "\n";

    cout << "Please enter your first and second names: ";

    string first;
    string second;

    cin >> first >> second;

    string name = first + " " + second;
    cout << "Hello, " << name << "\n";
    cout << "\n";

    cout << "Please enter two names: ";

    cin >> first >> second;

    if (first == second) cout << " that's the same name twice\n";

    if (first < second) cout << first << " is alphabetically before " << second << "\n";

    if (first > second) cout << first << " is alphabetically after " << second << "\n";

    return 0;
}