#include "std_lib_facilities.h"

int main()
{
    cout << "Enter your name: ";
    string sender_name;
    cin >> sender_name;

    cout << "Enter the name of the person you wanto to write to: ";
    string recipient_name;
    cin >> recipient_name;

    cout << "Enter the name of another friend: ";
    string friend_name;
    cin >> friend_name;

    char friend_sex = 0;
    cout << "Enter \"m\" if the friend is male and \"f\" if the friend is female: ";
    cin >> friend_sex;

    cout << "Enter recipient age: ";
    int age;
    cin >> age;
    if (age <= 0 || age > 120) {
        simple_error("You are kidding!");
    }

    cout << "\n";

    cout << "Dear " << recipient_name << ",\n";
    cout << "How are you? I am fine, but a I miss you.\n";
    cout << "I hope that we will be able to see each other soon.\n";
    cout << "How is Bilu? Is he as cute as ever?\n";
    cout << "Have you seen " << friend_name << " lately?\n";

    if (friend_sex == 'm') {
        cout << "If you see " << friend_name << " please ask him to call me.\n";
    } else if (friend_sex == 'f') {
        cout << "If you see " << friend_name << " please ask her to call me.\n";
    }

    cout << "I hear you just had a birthday and you are " << age << " years old.\n";

    if (age < 12) {
        cout << "Next year you will be " << age + 1 << "!\n";
    } else if (age == 17) {
        cout << "Next year you will be able to vote!\n";
    } else if (age > 70) {
        cout << "I hope you are enjoying retirement!\n";
    }

    cout << "Yours sincerely, \n\n";
    cout << sender_name << "\n";
    cout << "\n";

    return 0;
}